<?php

function astrum_dequeue_script() {
  wp_dequeue_script('superfish');
  wp_dequeue_script('jpanelmenu');
  wp_dequeue_script('custom');
  wp_deregister_script('custom');
  wp_enqueue_script( 'custom', get_stylesheet_directory_uri() . '/js/custom.js', array( 'jquery' ), '', true );
    wp_localize_script( 'custom', 'astrum',
    array(
        'ajaxurl'=>admin_url('admin-ajax.php'),
        'nonce' => wp_create_nonce('ajax-nonce'),
        'flexslidespeed' => ot_get_option('pp_flex_slideshowspeed',7000),
        'flexanimspeed' => ot_get_option('pp_flex_animationspeed',600),
        'flexanimationtype' => ot_get_option('pp_flex_animationtype','fade'),
        'breakpoint' => ot_get_option('pp_menu_breakpoint','767'),
        'sticky' => ot_get_option('pp_sticky_menu','true')
        )
    );
}
add_action( 'wp_print_scripts', 'astrum_dequeue_script', 100 );


class Walker_Nav_Menu_Dropdown extends Walker_Nav_Menu{
    var $to_depth = -1;
    private $curItem;
    function start_lvl(&$output, $depth = 0, $args = array()){
             $id = $this->curItem->ID;
             $linkrole = get_post_meta( $id, '_menu-item-linkrole', true);
             $types  = array( 'paragraph', 'titleh4', 'titleh5' );
             if(!in_array( $linkrole, $types )) {

              $output .= '</option>';
          }
     }

  function end_lvl(&$output, $depth = 0, $args = array()){
      $indent = str_repeat("\t", $depth); // don't output children closing tag
  }

  function start_el(&$output, $item, $depth = 0, $args = array(), $current_object_id = 0){
      $this->curItem = $item;
      $indent = ( $depth ) ? str_repeat( "&nbsp;", $depth * 4 ) : '';
      $class_names = $value = '';
      $classes = empty( $item->classes ) ? array() : (array) $item->classes;
      $classes[] = 'menu-item-' . $item->ID;
      $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );
      $class_names = ' class="' . esc_attr( $class_names ) . '"';
      $id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args );
      $id = strlen( $id ) ? ' id="' . esc_attr( $id ) . '"' : '';
      $value = ' value="'. $item->url .'"';
      $linkrole = get_post_meta( $item->ID, '_menu-item-linkrole', true);
      $types  = array( 'paragraph', 'titleh4', 'titleh5' );
      $args = (object) $args;
      if(!in_array( $linkrole, $types )) {
          $output .= '<option'.$id.$value.$class_names.'>';
          $item_output = $args->before;
          $item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
          $output .= $indent.$item_output;
      }
  }

  function end_el(&$output, $item, $depth = 0, $args = array()){
     $linkrole = get_post_meta( $item->ID, '_menu-item-linkrole', true);
     if($linkrole != 'paragraph' || $linkrole != 'titleh4' || $linkrole != 'titleh5') {
      if(substr($output, -9) != '</option>') {
            $output .= "</option>"; // replace closing </li> with the option tag

        }
        }
    }
}

?>